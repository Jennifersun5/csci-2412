<!-- object that stores all article info -->
<?php
class Article
{
  // parameters
  public $conn;
  public $articleId;
  public $title;
  public $author;
  public $content;
  public $publishDate;
  public $isPublished;

  function __construct($conn, $articleInfo)
  {
    $this->conn = $conn;
    $this->articleId = $articleInfo['articleId'];
    $this->title = $articleInfo['title'];
    $this->author = $articleInfo['author'];
    $this->content = $articleInfo['content'];
    $this->publishDate = $articleInfo['publishDate'];
    $this->isPublished = $articleInfo['isPublished'];
  }

  function __destruct()
  {
  }
  // setting a limit to # of articles pulled from the DB and accessing it
  static function getArticlesFromDb($conn, $numArticles = 20)
  {
    $selectArticles = "SELECT articles.*, users.fullName AS author
    FROM articles
    LEFT JOIN users ON users.userID=articles.authorId 
    WHERE isPublished = 1
    ORDER BY publishDate ASC
    LIMIT :numArticles";
    $stmt = $conn->prepare($selectArticles);
    $stmt->bindParam(':numArticles', $numArticles, PDO::PARAM_INT);
    $stmt->execute();

    $articleList = array();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
      $article = new Article($conn, $listRow);
      $articleList[] = $article;
    }

    return $articleList;
  }
  // using alias to grab single article given articleId
  static function getArticleById($conn, $articleId)
  {
    $selectArticlesById = "SELECT articles.* ,users.fullName AS author
    FROM articles
    LEFT JOIN users 
    ON users.userID=articles.authorId
    WHERE articleId = articles.authorId
    LIMIT :numArticles";
    $stmt = $conn->prepare($selectArticlesById);
    $stmt->bindParam(':numArticles', $articleId, PDO::PARAM_INT);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
      $article = new Article($conn, $listRow);
    }

    return $article;
  }
}
?>