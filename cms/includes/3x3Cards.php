<div class="container">
  <div class="row">
    <?php
    $data = Article::getArticlesFromDb($conn, 9);
    foreach ($data as $article) {
    ?>)
    <div class="col-4">
      <!-- route article based on articleId to new page -->
      <a class="card-wrapper" href="articlePage.php?articleId=<?php echo $article->articleId ?>">
        <div class="card">
          <h2><?php echo $article->author ?></h2>
          <p><?php echo $article->title ?></p>
          <p><?php echo $article->publishDate ?></p>
        </div>
    </div>
  <?php
    }
  ?>
  </div>
</div>