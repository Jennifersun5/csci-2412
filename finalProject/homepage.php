<!doctype html>
<html lang="en">
  <meta name="viewport" content="width=device-width, initial-scale=1"> <!--fit to scale -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.rtl.min.css" integrity="sha384-+qdLaIRZfNu4cVPK/PxJJEy0B0f3Ugv8i482AKY7gwXwhaCroABd086ybrVKTa0q" crossorigin="anonymous">
<?php
  include("../finalProject/navBar.php");
?>
<!-- add header component to navbar and include above -->
  </header>
  <div class="container-fluid pb-3">
    <div class="d-grid gap-3" style="grid-template-columns: 1fr 2fr 1fr;">
      <div class="bg-light border rounded-3" style="text-align:center">
        <br><br><br><br><br><span>Create a Jem</span><br><br><br><br><br>
      </div>
      <div class="bg-light border rounded-3" style="text-align:center">
        <br><br><br><br><br><span>Update Jem</span><br><br><br><br><br>
      </div>
      <div class="bg-light border rounded-3" style="text-align:center">
        <br><br><br><br><br><span>Share Jem</span><br><br><br><br><br>
      </div>
    </div>
  </div>
  <div class="d-grid gap-3" style="grid-template-columns: 1fr 2fr 1fr;">
    <div class="bg-light border rounded-3">
      <br><br><br><br><br><br><br><br><br><br>
    </div>
    <div class="bg-light border rounded-3">
      <br><br><br><br><br><br><br><br><br><br>
    </div>
    <div class="bg-light border rounded-3">
      <br><br><br><br><br><br><br><br><br><br>
    </div>
  </div>
</div>
  <?php include("../finalProject/footer.php");?>
  </html>