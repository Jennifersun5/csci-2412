DROP DATABASE IF EXISTS finalProjectKrisKettendorf;
CREATE DATABASE finalProjectKrisKettendorf;

USE finalProjectKrisKettendorf;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  userId INT PRIMARY KEY AUTO_INCREMENT,
  userName VARCHAR(25),
  userPassword VARCHAR(30)
);

INSERT INTO users (userName, userPassword)
VALUES ("jbeclectic", "Iamtheadmin"), ("bobbO43", "123easypeezy");

SELECT * FROM users;

DROP TABLE IF EXISTS contactInfo;
CREATE TABLE contactInfo (
  contactInfoId INT PRIMARY KEY AUTO_INCREMENT,
  userName VARCHAR(40),
  firstName VARCHAR(40),
  lastName VARCHAR(40),
  emailAddress VARCHAR(40),
  streetAddress VARCHAR(40),
  areaOfInterest VARCHAR(20),
  FOREIGN KEY (contactInfoId) REFERENCES users(userId) ON DELETE CASCADE
);

INSERT INTO contactInfo (userName, fullName, emailAddress, streetAddress, areaOfInterest)
VALUES 
("jbeclectic", "Kris", "Kettendorf", "kkettendorf@hotmail.com", "Somewhere In Delaware", "Columbus, Ohio"),
("rclower", "Robin", "Clower", "rclower@cscc.edu", "Delaware Hall 202", "Columbus Ohio");

SELECT * FROM contactInfo;

DROP TABLE IF EXISTS content;
CREATE TABLE content (
  contentId INT PRIMARY KEY AUTO_INCREMENT,
  resourceName VARCHAR(255),
  resourceLink VARCHAR(255),
  FOREIGN KEY (contentId) REFERENCES users(userId) ON DELETE CASCADE
);

INSERT INTO content (resourceName, resourceLink)
VALUES ("Top 50 Recycling Sites", "https://www.greenmatch.co.uk/blog/2015/07/top-50-recycling-sites"),
("Earth Recycling 911", "https://search.earth911.com/");

SELECT * FROM content;
-- *MVP: Fill out a form and receive insight on recycling locations closest to you*..."sign in to save info, send to phone" --> autofill info on sign in page?  (extra) ability to save locations, search bar for recyclable items linked to directory
-- Infographic 'directory' of most common recyclable items. instructions on how to recycle them and locations that will accept those materials near you (Embedded Web App + Widgets: Search, Index, )
-- What I want this app to be: all encompassing one-stop-shop ultimate-resource to recycling and also an informative educational resource