<?php
include("../cms/includes/utils.php");
$conn = connect_to_db("midtermKris");
$reviewText = "";
$numStars = "";
?>
<html>
<style>
  .error {
    color: #FF0000;
  }
</style>
<p><span class="error">(*Required field)</span></p>

<body>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="reviewText">Please Enter Your Review:</label>
    <input type="text" name="reviewText" id="reviewText" required><br><br>

    <label for="ratingReview">What Would You Rate This Product?</label> <br>
    <input type="radio" name="rating" value="5" required="required">(5)★★★★★</input> <br>
    <input type="radio" name="rating" value="4">(4)★★★★</input> <br>
    <input type="radio" name="rating" value="3">(3)★★★</input> <br>
    <input type="radio" name="rating" value="2">(2)★★</input> <br>
    <input type="radio" name="rating" value="1">(1)★</input> <br>

    <br><input type="submit" value="Submit"> <br>
  </form>

</body>
<?php

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  if (clean_input($_POST["reviewText"])) {
    $reviewText = clean_input($_POST['reviewText']);
  }
  if (clean_input($_POST["rating"])) {
    $numStars = clean_input($_POST["rating"]);
  }
}
if (!empty($reviewText)) {
  addNewReview($conn, $reviewText, $numStars);
}

if (isset($_GET['reviewText'])) {
  addNewReview($conn, $_GET['reviewText'], $_GET["rating"]);
} elseif (isset($_GET['deleteReviewId'])) {
  deleteReview($conn, $_GET['deleteReviewId']);
}

function addNewReview($conn, $reviewText, $numStars)
{
  $insert = "INSERT INTO reviews (reviewText, numStars) 
  VALUES (:reviewText, :numStars)";
  $stmt = $conn->prepare($insert);
  $stmt->bindParam(':reviewText', $reviewText);
  $stmt->bindParam(':numStars', $numStars);
  $stmt->execute();
}

function deleteReview($conn, $reviewId)
{
  $delete = "DELETE FROM reviews WHERE reviewId=:reviewId";
  $stmt = $conn->prepare($delete);
  $stmt->bindParam(':reviewId', $reviewId);
  $stmt->execute();
}

function printToDoList($conn)
{
  $selectItem = "SELECT * FROM reviews";
  $stmt = $conn->prepare($selectItem);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach ($stmt->fetchAll() as $listRow) {
    echo "<div class='reviewText row'>";
    $reviewId = $listRow['reviewId'];
    $reviewText = $listRow['reviewText'];
    $numStars = $listRow['numStars'];
    echo "
        <p class='col-4'>Review: <br> $reviewText ";
    for ($i = 0; $i <= $numStars - 1; $i++) {
      echo "★";
    }
    echo "  
        <a class='btn btn-delete col-1' href='reviews.php?deleteReviewId=$reviewId'><br>Delete</a></br>";
    // print out number of stars based on selection
  }
}

printToDoList($conn);

// Rating: ★

?>

</html>