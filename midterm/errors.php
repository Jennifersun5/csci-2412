<html>
<!--
  There are at least 8 things that should be changed about this code.
  Some of them will prevent the program from running,
  others are stylistic/good coding practices.
  The output you should see in your browser is
  "Ludwig van Beethoven has a famous song called Ode to Joy"
  Hint: Double check that your if statement works by changing the value for $x
  -->

<?php
$x = "Test code";
$y = "Ode to Joy";


if ($x = "Ludwig van Beethoven") {
  echo "$x has a famous song called $y";
}
?>

</html>

<!-- 
  wrapped in php...wrapped php in html tag...semicolon after y variable...echo statement in " "...changed value of 'x' to ensure if statement works...changed commenting to match html...
-->