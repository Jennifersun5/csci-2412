<?php
include("../cms/includes/navbar.php");
$conn = connect_to_db("toDoList");

$itemValue = getItemValue($conn, $_GET['editedItemId']); //grabbing itemValue attribute in DB and using it in our fx
$isComplete = getItemisComplete($conn, $_GET['editedItemId']);
?>

<html>
<link rel="stylesheet" href="toDoList.css">
<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="toDo">To Do:</label>
    <input type="text" name="toDo" id="toDo" value="<?php echo $itemValue ?>"><br>

    <input type="checkbox" name="isComplete" id="isComplete" value="<?php echo $isComplete ?>">
    <label for="isComplete">Is Done</label>
    <input type="submit" class="btn btn-primary" value="Submit">
  </form>
</div>

</html>

<?php
if ($_SERVER["REQUEST_METHOD" == "POST"]) {
  echo "here";
  if (clean_input($_POST["toDo"])) {
    $toDoItem = clean_input($_POST['toDo']);
  }
  if (isset($_POST['isComplete']) && $_POST['isComplete']) {
    $isComplete = true;
  }
  editToDoListItem($conn, $_GET['editedItemId'], $isComplete, $toDoItem);
  header("Location: toDoList.php");
}

function editToDoListItem($conn, $itemId, $isComplete, $toDoItem)
{
  $edit = "UPDATE items SET isComplete = :isComplete, toDoItem=:toDoItem 
  WHERE itemId=:itemId";
  $stmt = $conn->prepare($edit);
  $stmt->bindParam(':itemId', $itemId);
  $stmt->bindParam(':isComplete', $isComplete);
  $stmt->bindParam(':toDoItem', $toDoItem);
  $stmt->execute();
}

function getItemValue($conn, $itemId)
{
  $selectItem = "SELECT * FROM items WHERE itemId=$itemId";
  $stmt = $conn->prepare($selectItem);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach ($stmt->fetchAll() as $listRow) {
    return $listRow['toDoItem'];
  }
}

function getItemisComplete($conn, $itemId)
{
  $selectItemisComplete = "SELECT * FROM items WHERE itemId=$itemId";
  $stmt = $conn->prepare($selectItemisComplete);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach ($stmt->fetchAll() as $listRow) {
    return $listRow['isComplete'];
  }
}
?>

<!-- 
Once they've clicked "Submit" at the bottom of the form, they should route back to the main toDoList page with all of the 
information updated in the database.
You can make use of the header() PHP method to route back to the original form after you've done all of your error checking/POST logic.
-->