<?php
include("../cms/includes/navbar.php");
?>

<link rel="stylesheet" href="toDoList.css">

<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="toDo">To Do:</label>
    <input type="text" name="toDo" id="toDo"><br>

    <input type="checkbox" name="isComplete" id="isComplete" value="true">
    <label for="isComplete">Is Done</label>
    <input type="submit" class="btn btn-primary" value="Submit">
  </form>

  <?php
  $conn = connect_to_db("toDoList");

  $toDoItem = "";
  $isComplete = false;

  // sanitize toDo input and mark as complete
  if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (clean_input($_POST["toDo"])) {
      $toDoItem = clean_input($_POST['toDo']);
    }
    if (isset($_POST['isComplete']) && $_POST['isComplete']) {
      $isComplete = true;
    }
  }
  if (!empty($toDoItem)) {
    addToDoListItem($conn, $toDoItem, $isComplete);
  }
  // store info in URL to use inside page
  if (isset($_GET['completedItemId'])) {
    completedToDoListItem($conn, $_GET['completedItemId']);
  } elseif (isset($_GET['deletedItemId'])) {
    deleteToDoListItem($conn, $_GET['deletedItemId']);
  }
  // print from DB
  printToDoList($conn);

  function printToDoList($conn)
  {
    $selectItem = "SELECT * FROM items";
    $stmt = $conn->prepare($selectItem);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
      echo "<div class='toDoListItem row'>";
      $itemId = $listRow['itemId'];
      $itemValue = $listRow['toDoItem'];
      $complete = $listRow['isComplete'] ? 'Done' : 'To Do';
      if ($complete === "To Do") {
        echo "
        <p class='col-2'>$complete</p>
        <p class='col-2'>$itemValue</p>
        <a class='btn btn-edit col-1' href='editToDoList.php?editedItemId=$itemId'>Edit</a></br>
        <a class='btn btn-delete col-1' href='toDoList.php?deletedItemId=$itemId'>Delete</a></br>";
      } else {
        echo "
        <p class='col-2'>$complete</p>
        <p class='col-2'>$itemValue</p>
        <a class='btn btn-success col-1' href='toDoList.php?completedItemId=$itemId'>Done</a></br>
        <a class='btn btn-delete col-1' href='toDoList.php?deletedItemId=$itemId'>Delete</a></br>";
      }
      echo "</div>";
    }
  }

  function addToDoListItem($conn, $item, $isComplete)
  {
    $insert = "INSERT INTO items (toDoItem, isComplete) 
    VALUES (:item, :isComplete)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':item', $item);
    $stmt->bindParam(':isComplete', $isComplete, PDO::PARAM_BOOL);
    $stmt->execute();
  }

  function completedToDoListItem($conn, $itemId)
  {
    $update = "UPDATE items
      SET isComplete = true
      WHERE itemId=:itemId";
    $stmt = $conn->prepare($update);
    $stmt->bindParam(':itemId', $itemId);
    $stmt->execute();
  }

  function deleteToDoListItem($conn, $itemId)
  {
    $delete = "DELETE FROM items WHERE itemId=:itemId";
    $stmt = $conn->prepare($delete);
    $stmt->bindParam(':itemId', $itemId);
    $stmt->execute();
    // echo $itemId;
  }

// a foreach loop is a control flow statement for traversing items in a collection
// executes a provided function once for each array
// https://www.php.net/manual/en/control-structures.foreach.php
