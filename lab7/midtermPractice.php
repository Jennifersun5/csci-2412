<?php
include("../cms/includes/utils.php");

for ($i = 20; $i > 10; $i--) {
  echo "decreasing to the number 10: countdown $i</br>";
}

$countdown = [20, 19, 18, 17, 16, 15, 14, 13, 12, 11];

foreach ($countdown as $i) {
  echo "</br> decreasing to the number 10: starting at $i</br>";
}

$startingHands = ["AA", "KK", "QQ", "JJ", "TT"];
foreach ($startingHands as $p) {
  echo "The top five starting hands in poker:
  </br>$p";
}
// the top five starting hands in poker are: "AA" "KK" "QQ" "JJ" "TT"...
// if index === 0 echo "The top hand in poker is Pocket Aces" ($p)...(cycle through indexes and output a different message according to position-- switch statement in foreach?)
// 

function addNewItem($conn, $itemId)
{
}

/*
Create loops that will print out different items depending on what value the iterator ($i) is. 
For instance, I might have you print out if your iterator is odd/even, or style a div differently 
if it's a multiple of 3.
*/

function isOddOrEven()
{
  for ($i = 0; $i < 10; $i++) {
    if ($i % 3 == 0) {
      echo "<div style= color:red>$i</div>";
    } elseif ($i % 2 == 0) {
      echo "$i is an even number.";
    }
  }
}
isOddOrEven();

/*
Create loops that will iterate through an array and print out all of its contents.
*/

$daysOfTheWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

foreach ($daysOfTheWeek as $day) {
  if ($day === $daysOfTheWeek[0]) {
    echo "Today is $day. The start of the week for most! <br>";
  } elseif ($day === $daysOfTheWeek[4]) {
    echo "It's $day. Time for the weekend! <br>";
  } else echo "Today is $day. <br>";
}

echo $daysOfTheWeek[1] . "<br>";
count($daysOfTheWeek);
sort($daysOfTheWeek);

/*
Create functions that will accept different numbers of parameters and run them successfully. 
You should be able to handle cases where you're taking no parameters, 1 parameter, or 2+ parameters. 
You should also be prepared to return outputs from functions. 
For instance, I might ask you to create a function that will multiply 3 variables together and return the product. (lab3.php)
*/

// Overview:

function functionName($param)
{
  echo "This is where we output a param: $param";
}

functionName("This is an Input Value <br>");

function cubed($number)
{
  $cubed = $number * $number * $number;
  return $cubed;
}

function threeNumbers($a, $b, $c)
{
  $sumOfThree = $a + $b + $c;
  return $sumOfThree;
}

threeNumbers(6, 7, 8);

// Additonal Practice:
// PHP Variable Length Argument Function
// initialize sum
// loop through iterator
// output/return
function mathAndNumbers(...$numbers)
{
  $sum = 0;
  foreach ($numbers as $n) {
    $sum += $n;
  }
  return $sum;
}

echo mathAndNumbers(1, 5, 82, 6, 16);

/*
You should be able to catch cases where someone provides an input that would normally cause an exception and 
handle it gracefully. 
For instance, if I ask you to create a function to divide two numbers and the denominator is 0, I would expect you to 
throw a new divide by 0 Exception.
The functional call should also be wrapped in a try/catch block which would output the message of the Exception & 
the line number it occurred on.
*/

function divideByZero($dividend, $divisor)
{
  if ($divisor == 0) {
    throw new error("Cannot divide by 0.");
  }
  return $dividend / $divisor;
  try {
    echo divideByZero(5, 0);
  } catch (error $er) {
    $message = $er->getMessage();
    $file = $er->getFile();
    $line = $er->getTraceAsString();
    echo "$message $file $line";
  } finally {
    echo "Process complete.";
  }
}

/*
Given a set of input fields, you should be able to create a form, output the result of the fields, prevent any 
malicious/bad input, and validate user input. 
You should be able to check for is_numeric, etc and obey all accessibility requirements associated with forms. 
*/

?>

<!-- Create a form, output information -->
<html>
<form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
  <table>
    <tr>
      <td>Name:</td>
      <td> <input type="text" name="name" /></td>
    </tr>
    <tr>
      <td>Password:</td>
      <td> <input type="password" name="password" /></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" value="login" /> </td>
    </tr>
  </table>
</form>

</html>
<?php

$name = $_POST["name"];
$password = $_POST["password"];

echo "Hello: $name, your password is $password";

?>
<!-- 
For instance, I might ask you to create a form with a select box for a set of movies, a radio button selection for
the size of popcorn, and a numeric textbox for the number of movie tickets and you should be able to output how much
money it will cost to go see the selected movie by multiplying a ticket price by the number of tickets and adding in
the cost of popcorn.  
-->
<html>
<form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
  <label for="movie-label">Movies:</label>
  <select name="movies" id="movies">
    <option value="">--Please choose an option--</option>
    <option value="movie-title">Man On Fire</option>
    <option value="movie-title">Training Day</option>
    <option value="movie-title">Deja Vu</option>
  </select>
  <br>
  <label for="">Size of Popcorn:</label>
  <select name="popcorn-size" id="">
    <option value="">--Please choose an option--</option>
    <option value="size-sml">Small</option>
    <option value="size-med">Medium</option>
    <option value="size-lrg">Large</option>
  </select>
  <br>
  <label for="num-of-movies-tickets">Number of Movie Tickets:</label>
  <input type="number" name="movieTicketNumber" id="num-of-movies-tickets" min="1" max="20">
  <input type="submit">
</form>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["movie-ticket-number"])) {
    $numErr = "Please enter a number";
  } elseif (!is_numeric($_POST["movie-ticket-number"])) {
    $numErr2 = "Number is required for this field";
  } else {
    $movieTicketNumber = clean_input($_POST["movie-ticket-number"]);
  }
}

$movieTicketNumber = $_POST["movieTicketNumber"];
$price = 6.00;
$totalCost = $movieTicketNumber * $price;

echo "Total Cost for tickets is: $$totalCost";

?>
<!-- 
You should be able to write a script that will create a new database, tables, and populate those tables with valid input. 
You should be able to select information from your tables and output that information.
For instance, I might ask you to create a new database called City, and have one table called Addresses. 
Addresses would have information such as a primary key, street number, street name, city, & zip. 
You should be able to create a query that will print various address information, like print all addresses in Columbus. 


-- CREATE DATABASE City;

USE City;
DROP TABLE IF EXISTS Addresses;

-- CREATE new db with table
CREATE TABLE Addresses (
	addressId INT PRIMARY KEY AUTO_INCREMENT,
	streetNumber INT,
    streetName VARCHAR(30),
    cityName VARCHAR(30),
    zipCode INT
);
-- Insert values
INSERT INTO Addresses (streetNumber, streetName, cityName, zipCode)
VALUES 
("1460", "West Side Story", "Columbus", "12346"),
("6063", "Ullamcorper Street", "Roseville", "11523");
 
UPDATE Addresses
SET streetName = "West Side Story Lane"
WHERE addressId = 1;

DELETE FROM Addresses WHERE addressId = 2;
SELECT * FROM Addresses;
-->

</html>