<html>
<link rel="stylesheet" href="breakpoints.css">
<?php
include("../lab5/cms/navbar.php");
?>

<body>
  <h1>This is showing mobile first development</h1>
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-4" style="background-color: azure;">
        <h2>Column 1</h2>
      </div>
      <div class="col-12 col-md-4" style="background-color: bisque;">
        <h2>Column 2</h2>
      </div>
      <div class="col-12 col-md-4" style="background-color: coral;">
        <h2>Column 3</h2>
      </div>
      <div class="col-6 col-md-3" style="background-color: magenta;">Column 1</div>
      <div class="col-6 col-md-3" style="background-color: coral;">Column 2</div>
      <div class="col-6 col-md-3" style="background-color: bisque;">Column 3</div>
      <div class="col-6 col-md-3" style="background-color: azure;">Column 4</div>
    </div>
  </div>
</body>

</html>
<!-- 
columns span to 1/2 of what the total is
<div class="col-6 col-lrg-12" style="background-color: magenta;">Column 1</div>
<div class="col-6 col-lrg-12" style="background-color: coral;">Column 2</div> 
-->