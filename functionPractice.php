<?php

// Use of return

function square($num) {
  return $num * $num;
}

square(4); //16

?>
<!--Resource: https://www.php.net/manual/en/functions.returning-values.php -->