<?php
class Article
{
  protected $header;
  protected $summary;
  protected $fullText;

  function __construct($header, $summary, $fullText)
  {
    $this->header = $header;
    $this->summary = $summary;
    $this->fullText = $fullText;
  }

  function __destruct()
  {
  }

  public function render()
  {
    $card = "
      <div class=\"article\">
          <h1>$this->header</h1>
          <p>$this->fullText</p>
          <table>$this->summary</table>
      </div>";
    echo $card;
  }
}

$fullArticle = new Article("This is a full article", "summary for full article", "full paragraph text for full article");
$darkKnight = new Article("This is the full article on the Dark Knight", "this is the summary of the article", "full paragraph text for full article");
$oscarsArticle = new Article("This is the article header for the Oscars", "This is the summary section for the Oscars", "This is the full text section for the Oscars");
?>
<link rel="stylesheet" href="article.css">

<div class="container">
  <?php $fullArticle->render() . "<br>"; ?>
  <?php $darkKnight->render() . "<br>"; ?>
  <?php $oscarsArticle->render() . "<br>"; ?>

</div>

<!-- 
properties
construct
destruct
render fx
create new objects
render in container to web
 -->