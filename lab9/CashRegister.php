<?php

class CashRegister
{
  protected $amountInRegister = 0;

  function __construct($amountInRegister)
  {
    $this->amountInRegister = $amountInRegister;
  }
  function __destruct()
  {
  }
  // overrides constructor
  function set_AmountInRegister($amountInRegister)
  {
    $this->amountInRegister = $amountInRegister;
  }
  function get_AmountInRegister()
  {
    return "Amount in register is: $$this->amountInRegister";
  }
  function addMoney($amountDeposited)
  {
    $this->amountInRegister += $amountDeposited;
    echo "Amount deposited: $$amountDeposited" . "<br> Bringing your amount up to: $$this->amountInRegister!<br>";
  }
  function removeMoney($amountWithdrawn)
  {
    if ($amountWithdrawn > $this->amountInRegister) {
      echo "Insufficient funds: unable to withdraw $$amountWithdrawn, please select a different amount.<br>";
    } else {
      $this->amountInRegister -= $amountWithdrawn;
      echo "<br> Amount withdrawn: $$amountWithdrawn" . "<br> Remaining Balance: $$this->amountInRegister <br>";
    }
  }
}
