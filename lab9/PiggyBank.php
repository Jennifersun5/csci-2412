<?php
include("CashRegister.php");

class PiggyBank extends CashRegister
{
  function removeMoney($amountWithdrawn)
  {
    echo "Removing money from PiggyBank: You can't remove money from the piggy bank without smashing it! <br>";
  }
  function breakBank()
  {
    echo "We smashed the Piggy Bank and removed $$this->amountInRegister from inside." . "<br>";
    $this->set_amountInRegister(0);
  }
}
