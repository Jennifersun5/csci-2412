<?php
// Variables => Properties, Functions => Methods
// public = access modifier protected is preferred ()
class Animal
{
  // Properties
  public $name, $age;
  // constructor method to set name and age properties
  function __construct($name, $age)
  {
    $this->name = $name;
    $this->age = $age;
  }
  function __destruct()
  {
    // echo "$this->name is no longer being used, so it's getting destroyed </br>";
  }
  // Methods
  // function set_name($name)
  // {
  //   $this->name = $name;
  // }
  function get_name()
  {
    return $this->name;
  }
  // function set_age($age)
  // {
  //   $this->age = $age;
  // }
  function get_age()
  {
    return $this->age;
  }
  function echoNameandAge()
  {
    echo "$this->name is $this->age years old <br>";
  }
}

$fido = new Animal("Fido", 6);
// $fido->set_name("Fido");
// $fido->set_age("6");
echo $fido->echoNameandAge();

$clarence = new Animal("Clarence", 3);
// $clarence->set_name("Clarence");
// $clarence->set_age("3");
// echo $clarence->get_name() .  "<br>";
// echo $clarence->get_age() . "<br>";
echo $clarence->echoNameandAge();


$karen = new Animal("Karen", 47);
// $karen->set_name("Karen");
// $karen->set_age("47");
// echo $karen->get_name() . "<br>";
// echo $karen->get_age();
echo $karen->echoNameandAge();
