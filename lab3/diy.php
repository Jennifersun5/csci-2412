<?php 

function multiplyby37($sumnum) {
  if(!is_Numeric($sumnum)) {
    throw new Exception("Is $sumnum a number??");
  }
  $by37 = $sumnum * 37;
  return "$sumnum * 37 = $by37 </br>";
}

try {
  // echo multiplyby37("Hello");
  // echo multiplyby37(true);
  echo multiplyby37(6);
}

catch (Exception $ex){
  $message = $ex->getMessage();
  $file = $ex->getFile();
  $line = $ex->getTraceAsString();
  echo "$message $file $line";
}
finally {
  echo "Process complete.</br>";
}

// puzzle
// find all multiples of 3 and 5 below 1000
// iterate through numbers below 1000;
// identify those that are multiple (meaning divisible) by 3 or 5 (modulo)
// output those numbers in the browser

$sum = 0;

for ($i=0; $i < 10; $i++) {
  if($i %3==0) {
   echo "$i is a multiple of 3 </br>";
   $sum += $i;
  } elseif($i %5==0) {
    echo "$i is a multiple of 5 </br>";
    $sum += $i;
  }
}

echo "</br>The sum of Euler's numbers are $sum";

?>

<!-- sass: CSS precompiler, learn a package manager, Cloud practitioner certificate -->