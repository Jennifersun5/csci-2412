<?php

// arrays
// an indexed list- basic syntax
$starwarsfilms = array("A New Hope", "The Empire Strikes Back", "Return of the Jedi");
// print
print_r($starwarsfilms);
// grab index of array
echo $starwarsfilms[1];
// length
echo count($starwarsfilms); // 3
// sort (alphabetical)
sort($starwarsfilms);
// for more operations: https://www.php.net/manual/en/ref.array.php
array_push($starwarsfilms, "The Phantom Menace");
unset($starwarsfilms[0]); //removes "A New Hope"

$topFiveStartingHandsPoker = array("Game: No-Limit Hold'em", "AA (Pocket Aces)", "KK (Pocket Kings)", "QQ (Pocket Queens", "JJ (Pocket Jacks)",  "AKs (Ace-King suited)" );

// can also make "associative arrays" (can be used to store user information (key) and details about that user (value))
$releaseDate = array("A New Hope"=> 1977, "The Empire Strikes Back"=> 1980, "Return of the Jedi"=> 1983);
echo $releaseDate["The Empire Strikes Back"] ."</br>";

// practice
$courseAndProfSpring22 = array("Introduction to Logic"=> "Newman, E", "Web Database Development"=> "Clower, R", "Web GIS"=> "Giza, J");
echo $courseAndProfSpring22["Web Database Development"] ."</br>";

// conditional logic: while, for, forEach

// while (while user is logged in change the login button to logout)
  $i = 0;
  while($i <= 5) {
    echo "The number is $i </br>";
    $i++;
  }
// for (used for a set amount of times) er: 10 items on a page, array of numbers, printing out a list of names
  for($i = 0; $i <= 5; $i++) {
    echo "The number is $i </br>";
  }
// practice (sheep)
  for($i = 0; $i <= 20; $i++) {
    echo "$i sheep, $i sheep are jumping over a fence: you are getting very sleepy... </br>";
  }

// forEach (used for iterating through arrays)
// looping through avengers heroes
$avengers = array("Thor", "Captain America", "Iron Man", "Black Widow");

foreach($avengers as $hero) {
  echo "<br> $hero is in the Avengers! </br>";
}

foreach($topFiveStartingHandsPoker as $topFive) {
  echo "</br> $topFive are among the Top 5 Starting Hands in Poker. </br>";
}

// functions (write once, use later) 
// passing parameters
function echoName($name) {
  echo "<br> Hello $name</br>";
}
echoName("Jackie");
echoName("Kris </br>");

// passing name as a parameter to dynamically print out a value

function divisibleby3or5($numbertest) {
  if ($numbertest %5==0 && $numbertest %3==0) {
    echo "$numbertest is divisible by 5 and 3";
  } elseif (!$numbertest %5==0 || !$numbertest %3==0) {
    echo "$numbertest is not divisible by 5 or 3";
  } elseif($numbertest %3==0) {
    echo "$numbertest is divisible by 3";
  } elseif ($numbertest %5==0) {
    echo "$numbertest is divisible by 5";
  } 
}

echo divisibleby3or5(30) ."</br>"; 
echo divisibleby3or5(21) ."</br>";
echo divisibleby3or5(62) ."</br>";

// fxs can have multiple params
function login($username, $password) {
  // log user in if username and pw match in database
}

login("Triggxl", "betyoucantguessthis");

// fxs also have the ability to return a value
function cube($numcubed) {
  //initiate var
  $cubed = $numcubed * $numcubed *$numcubed;
  // return it
  return $cubed;
}

// declare variables to call
$cubeof3 = cube(3);
$cubeof5 = cube(5);
$cubeof127 = cube(127);

echo ("<br>". $cubeof3 ."</br>");
echo ("<br>". $cubeof5 ."</br>");
echo ("<br>". $cubeof127 ."</br>");

function divideBy0($divide, $divisor) {
  if($divisor === 0) {
    throw new error("Can't divide by 0.");
  }
  return $divide / $divisor;
}
try {
  echo divideBy0(5,0);
}
catch (error $er){
  $message = $er->getMessage();
  $file = $er->getFile();
  $line = $er->getTraceAsString();
  echo "$message $file $line";
}
finally {
  echo "Process complete.";
}
?>
