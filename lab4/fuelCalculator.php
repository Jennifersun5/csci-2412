<!-- 
  define variables

-->

<html>
  <?php
      include("header.php");
     // variables
      $averageMilesDriven = $amdErr = $mpgResult = $mpgErr = $gasCost = "";
      $amountSpentOnGroceries = 0;

    // creating request && sanitizing data using clean_input fx
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["average-miles-driven"])) {
      $amdErr = "Please enter a number";
    } elseif (!is_numeric($_POST["average-miles-driven"])){
      $amdErr = "Number is required for this field";
    } else {
      $averageMilesDriven = clean_input($_POST["average-miles-driven"]);
    }
    if (empty($_POST["car-MPG"])) {
      $mpgErr = "Please enter a number";
    } elseif (!is_numeric($_POST["car-MPG"])){
      $mpgErr = "Number is required for this field";
    } else {
      $mpgResult = clean_input($_POST["car-MPG"]);
    }
      $gasCost = clean_input($_POST["gas-type"]);
    if(!is_numeric($_POST["grocery-bill"]) && !empty($_POST["grocery-bill"])) {
      $groceryErr = "Number is required for this field";
    } else {
      $amountSpentOnGroceries = clean_input($_POST["grocery-bill"]);
    }
  }

  // fuel discount should be 10 cents off every $100
  // discounted_price = original_price - (original_price * discount / 100)
  function apply_fuel_perks_discount($gasCost, $amountSpentOnGroceries) {
    $fuelPerks = floor(($amountSpentOnGroceries / 100)) * 0.1;
    echo "<br> You saved: $fuelPerks cents off of $$gasCost!";
    return $gasCost - $fuelPerks;
  }
  function calculateTotalGasCost($gasCost, $mpgResult, $amountSpentOnGroceries, $averageMilesDriven) {
    // get actual gasCost using applyfpd function
    $discountedCosts = apply_fuel_perks_discount($gasCost, $amountSpentOnGroceries);
    $totalGasCost = ($averageMilesDriven / $mpgResult) * $discountedCosts;
    // echo $averageMilesDriven, $mpgResult, $discountedCosts;
    return $totalGasCost;
  }

  function clean_input($data)
  {
    $data = trim($data); //removes whitespace
    $data = stripslashes($data); //removes slashes
    $data = htmlspecialchars($data); //replace html chars
    return $data;
  }

  ?>

  <style>
    .error {
      color: #FF0000;
    }
  </style>

<body>
  <h1>Fuel Calculator</h1>
    <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
      <p><span class="error">* required field</span></p>
      Average Miles Driven: <input type="text" name="average-miles-driven"><br>
      <span class="error">* <?php echo $amdErr;?></span><br><br>
      Enter Miles Per Gallon: <input type="text" name="car-MPG"><br>
      <span class="error">* <?php echo $mpgErr;?></span><br><br>
      Enter Amount Spent on Groceries: (for "Fuel Perks", Optional) <input type="text" name="grocery-bill"><br>
      <span class="error">* <?php echo $mpgErr;?></span><br><br>
    
      <label for="gas-type">Please Select Your Gas Type:</label> </br>
      <label for="Octane-87"></label><input type="radio" id="Octane-87" name="gas-type" value="1.89"> Octane-87 </br>
      <label for="Octane-89"></label><input type="radio" id="Octane-89" name="gas-type" value="1.99"> Octane-89 </br>
      <label for="Octane-92"></label><input type="radio" id="Octane-92" name="gas-type" value="2.09"> Octane-92 </br>
      
      <input type="submit"> <br>
      <br><p>Your Average Miles Driven: <?php echo $averageMilesDriven; ?></p>
      <p>Your car's Miles Per Gallon: <?php echo $mpgResult; ?></p>
      <p>Gas Type: <?php echo $gasCost; ?></p>
      <p><?php echo "<br> Total Gas Cost is: " . "$".calculateTotalGasCost($gasCost, $mpgResult, $amountSpentOnGroceries, $averageMilesDriven)  ?></p>
    </form>
    </body>
</html>
<!-- add echo's in code to find code that's not working properly
-->