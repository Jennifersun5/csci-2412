<html>
  
<?php

include("header.php");
$toggleParagraphs = false;

if ($toggleParagraphs) {
  echo "<p>Currently I'm displaying this paragraph</p>";
} else {
  echo "<p>Now I'm displaying a different one</p>";
}
for ($i = 0; $i < 10; $i++) {
  if ($i % 2 == 0) {
    echo "<div style=\"width:100px;height:100px;border:1px solid #000; display: inline-block; margin: 5px; background-color: white;\"></div>";
  } else {
    echo "<div style=\"width:100px;height:100px;border:1px solid #000; display: inline-block; margin: 5px; background-color: black;\"></div>";
  }
}

$name = $favoriteNumber = $fullNameErr = $numErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["fullName"])) {
    $fullNameErr = "Name is required";
  } else {
    $name = clean_input($_POST["fullName"]);
  }
  if (empty($_POST["number"])) {
    $numErr = "Number is required";
  } elseif (!is_numeric($_POST["number"])){
    $numErr = "Please enter a number";
  } else {
    $favoriteNumber = clean_input($_POST["number"]);
  }
  
}

function clean_input($data)
{
  $data = trim($data); //removes whitespace
  $data = stripslashes($data); //removes slashes
  $data = htmlspecialchars($data); //replace html chars
  return $data;
}
?>

<style>
  .error {
    color: #FF0000;
  }
</style>
<p><span class="error">* required field</span></p>

<body>
    <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="fullName">Name:</label>
    <input type="text" name="fullName" id="fullName" required>
    <span class="error">* <?php echo $fullNameErr;?></span><br><br>
    
    <label for="number">Favorite Number:</label>
    <input type="text" name="number" id="number" required><br>
    <span class="error">* <?php echo $numErr;?></span><br><br>

    
    <label for="cars">Choose a car:</label> <br>
    <input type="radio" name="cars"value="volvo">Volvo</input>
    <input type="radio" name="cars" value="saab">Saab</input>
    <input type="radio" name="cars" value="fiat">Fiat</input>
    <input type="radio" name="cars" value="audi">Audi</input> <br><br>

    <input type="submit" value="Submit"> <br>

        <p>Welcome <?php echo $name; ?><br></p>
        <p>Your favorite number is: <?php echo $favoriteNumber; ?></p>
        <p>Your car: <?php echo htmlspecialchars($_POST["cars"]); ?></p>
  </form>
</body>

</html>