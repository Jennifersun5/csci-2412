<?php
/*
	Quick reference: 
	concat = "."
	command L+O start live server
*/
	$firstName = "Kris";
	$lastName = "Kettendorf";
	$age = 12;
	$currentYear = 2022;
	$myActualAge = $currentYear - $age - 1;
	$isMarried = true;
	echo "$firstName $lastName </br>";
	echo $firstName . " frick'n " . $lastName . "</br>";
	echo "I was born in " . $myActualAge. "</br>";
	echo "I am married: $isMarried </br>"; //This will print a 1 or a 0, 1 means true, 0 means false

// Practice:

// Variables
echo "My first name is: $firstName </br>";
echo "$firstName...$lastName. </br>";
$fullName = $firstName . " " . $lastName;
echo "Nice to meet you, $fullName </br>";

// Operators (Modulo)
echo 8 % 3 ."</br>";
echo 15 % 4 ."</br>";
echo 4 % 9 ."</br>";
echo 7 % 2 ."</br>";

/*
	Modular arithmetic is useful for seeing if numbers are divisible evenly by another number. 
	For instance, if you wanted to check if a number is even, you could do modulo 2:
*/
echo 5 % 2 . " ". "(This result represents an odd number)</br>" ;
echo 10 % 2 . " ". "(This result represents an even number)</br>";

// There's also shorthand you can use if you want to do basic operations
$x = 8;
$y = 4;

echo $x += $y ."</br>"; //Addition
echo $x -= $y ."</br>"; //Subtraction

// Q: Not spacing, outputting ||
echo $x < $y ."</br>";
echo $x > $y ."</>";
echo $x && $y ."</br>";
echo $x || $y ."</br>";

// Conditional Logic

// "Generational Sorter"
$generation = "";
$birthYear = $age;
if ($birthYear < 1964) {
	$generation = "Boomer";
} elseif ($birthYear > 1964 && $birthYear < 1980) {
	$generation = "Gen X";
} elseif ($birthYear > 1980 && $birthYear < 1996) {
	$generation = "Millenial";
} elseif ($birthYear > 1996 && $birthYear < 2012) {
	$generation = "Gen Z";
// } elseif ($birthYear < 1964){
// 	$generation = "You are too young for a generation label"; 
} else {
	$generation = "Wer'e not quite sure what you are yet..";
}

echo "</br>" ."Generation: $generation" ."</br>";
// Q: any age < 1964 = boomer 88. included --> including
?>
